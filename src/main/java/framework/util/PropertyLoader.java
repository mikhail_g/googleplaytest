package framework.util;

import java.io.IOException;
import java.util.Properties;


public class PropertyLoader {
    private final static String PROP_FILE = "../../googleplaytest.properties";
    private int timeout,numTriesOfAppSelection;
    private String browserName, email, password;
    private Properties props;
    private static PropertyLoader propertyLoader = null;

    private PropertyLoader(){
        setProperties();
    }

    public static PropertyLoader getInstance(){
        if(propertyLoader == null){
            propertyLoader = new PropertyLoader();
        }
        return propertyLoader;
    }

    private void setProperties() {

        props = new Properties();
        try {
            props.load(PropertyLoader.class.getResourceAsStream(PROP_FILE));
        } catch (IOException e) {
            e.printStackTrace();
        }

        timeout = Integer.parseInt(props.getProperty("timeout"));
        numTriesOfAppSelection = Integer.parseInt(props.getProperty("num.tries"));
        browserName = props.getProperty("browser.name");
        email = props.getProperty("user.email");
        password = props.getProperty("user.password");
    }

    public int getTimeout() {
        return timeout;
    }

    public int getAppSelectionRetries() {return numTriesOfAppSelection; }

    public String getBrowserName() {
        return browserName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}