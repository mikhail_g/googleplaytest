package testclasses;

import framework.util.PropertyLoader;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import pages.GoogleComPage;

import java.util.concurrent.TimeUnit;
import static framework.webdriver.BrowserDriverFactory.getDriver;


public class TestBase {

    protected static WebDriver driver;
    private final String googleComUrl = "http://google.com";
//    private final String chromeExtensionsUrl = "https://chrome.google.com/webstore";
    private final String chromeExtensionsUrl = "https://chrome.google.com/webstore/category/app/69-office-applications";

    @BeforeSuite
    public void setUp() throws Exception {
        PropertyLoader propertyLoader = PropertyLoader.getInstance();
        driver = getDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(propertyLoader.getTimeout(), TimeUnit.SECONDS);
    }

    @AfterSuite
    public void quitDriver(){
        driver.quit();
    }

    protected GoogleComPage openGoogleComPage(){
        driver.get(googleComUrl);
        return new GoogleComPage(driver);
    }

    protected void openChromeWebStorePage(){driver.get(chromeExtensionsUrl);}
}
