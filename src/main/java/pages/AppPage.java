package pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;


public class AppPage extends Page {


    public AppPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".app-compatibility-final .compatibility-info-img")
    private WebElement compatibilityIcon;

    @FindBy (css = ".details-actions span.id-track-click")
    private WebElement alreadyInstalledButton;

    @FindBy(css = ".modal-dialog #purchase-ok-button")
    private WebElement submitInstallation;

    @FindBy(css = ".details-actions .price.buy.id-track-click")
    private WebElement installButton;

    @FindBy(css = ".modal-dialog #close-dialog-button")
    private WebElement submitCongratulations;


    public boolean isCompatible(){
        try {
            return compatibilityIcon.isDisplayed();
        }
        catch (NoSuchElementException e){return false;}
    }

    public boolean isInstalled(){
        return alreadyInstalledButton.isDisplayed();
    }

    public void installApp() {
        openInstallMenu();
        submitInstallation();
        submitCongratulations();
    }

    private void submitCongratulations() {
        wait.until(ExpectedConditions.visibilityOf(submitCongratulations));
        submitCongratulations.click();
    }

    private void submitInstallation() {
        wait.until(ExpectedConditions.visibilityOf(submitInstallation));
        submitInstallation.click();
    }

    private void openInstallMenu() {
        installButton.click();
    }
}
