package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class GoogleLoginPage extends Page {

    public GoogleLoginPage(WebDriver driver){
        super(driver);

        WebDriverWait wait = new WebDriverWait(driver, super.propertyLoader.getTimeout());
        wait.until(ExpectedConditions.elementToBeClickable(signIn));
    }

    @FindBy(id = "Email")
    private WebElement email;

    @FindBy(id = "Passwd")
    private WebElement passwd;

    @FindBy(id = "signIn")
    private WebElement signIn;

    public GoogleComPage executeLogin() {

        email.clear();
        email.sendKeys(super.propertyLoader.getEmail());

        passwd.clear();
        passwd.sendKeys(super.propertyLoader.getPassword());

        signIn.click();

        return new GoogleComPage(driver);
    }
}

