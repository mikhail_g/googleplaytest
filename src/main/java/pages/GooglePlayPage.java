package pages;

import framework.webdriver.BrowserDriverFactory;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;


public class GooglePlayPage extends Page {

    private final static String LOADER_LOCATOR = "#page-load-indicator.loaded";
    public GooglePlayPage(WebDriver driver){
        super(driver);
    }


    @FindBy(css = ".nav .apps>a")
    private WebElement filterByApps;

    public AppPage selectApp() throws Exception {
        filterByApps();
        for(int i=0;i< super.propertyLoader.getAppSelectionRetries();i++) {
            chooseRandomApp();
            AppPage appPage = new AppPage(driver);
            if (appPage.isCompatible()) {
                return appPage;
            }
            else{filterByApps();}
        }
        throw new Exception(super.propertyLoader.getAppSelectionRetries()+" TESTED APPS ARE NOT COMPATIBLE WITH YOUR DEVICE(THE NUMBER OF TRIES IS SET IN PROPERTIES FILE)");
    }

    private void chooseRandomApp(){
        try {
            String locator;
            if (super.propertyLoader.getBrowserName().equals(BrowserDriverFactory.CHROME_NAME)) {
                locator = ".card:not(.hidden-card):not(.medium-plus)>div";
            }
            else {
                locator = ".card:not(.hidden-card):not(.medium-plus)>div>a";
            }
            List<WebElement> apps = driver.findElements(By.cssSelector(locator));
            int random = (int) (Math.random() * (apps.size() - 1));
            System.out.println("The App #" + random + " was selected");
            WebDriverWait wait = new WebDriverWait(driver, super.propertyLoader.getTimeout());
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locator)));
            apps.get(random).click();
        } catch (ElementNotVisibleException e) {
            chooseRandomApp();
        }
    }

    private void filterByApps() {
        filterByApps.click();
        WebDriverWait wait = new WebDriverWait(driver, super.propertyLoader.getTimeout());
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(LOADER_LOCATOR)));

    }

}
