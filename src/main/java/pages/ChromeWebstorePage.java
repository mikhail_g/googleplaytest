package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class ChromeWebStorePage extends Page {

    WebDriverWait wait = new WebDriverWait(driver, super.propertyLoader.getTimeout());
    WebElement toBeInstalledApp;
    private final String appsLocator = ".webstore-S-cb-xc";

    public ChromeWebStorePage(WebDriver webDriver){super(webDriver);}


    @FindBy(css = ".webstore-qb-ac-Db-Cb-Ig-lc-Bc-apps")
    WebElement appsFilterButton;

    @FindBy(css = ".webstore-S-cb-xc")
    List<WebElement> apps;

    @FindBy(css = "div.webstore-ae-sc-F.webstore-ae-sc-Ee-F.webstore-ae-sc-F-nb-oe")
    private WebElement installButton;

    @FindBy(css = "div.webstore-ae-sc-cc.webstore-test-button-label")
    private WebElement anyStateInstallButton;

    @FindBy(css = "div.webstore-ae-sc-F.webstore-ae-sc-Ce-Pb-F.webstore-ae-sc-F-nb-oe")
    private WebElement alreadyInstalledButton;

    @FindBy(id = ":v-website")
    private WebElement websiteRadioButton;

    @FindBy(css = ".webstore-Rf-r-V")
    private WebElement chosenAppIcon;

    public void setFilters(){
        filterByApps();
        chooseWebsiteRadioButton();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
    }

    private void chooseWebsiteRadioButton() {
        websiteRadioButton.click();
    }

    public void installChromeApp() throws Exception {
        chooseRandomApp();
        for(int i=0;i< super.propertyLoader.getAppSelectionRetries();i++) {
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            if(isCompatible()){
                System.out.println("COMPATIBLE APP WAS SELECTED");
                try {
                    installButton.click();
                }
                catch (StaleElementReferenceException e){return;}
                return;
            }
            else{chooseRandomApp();}
        }
        throw new Exception(super.propertyLoader.getAppSelectionRetries()+" TESTED APPS ARE NOT COMPATIBLE WITH CHROME-DRIVER (THE NUMBER OF TRIES IS SET IN PROPERTIES FILE)");
    }

    public void chooseInstalledApp(){
        moveToElement(toBeInstalledApp);
        try {
            wait.until(ExpectedConditions.elementToBeClickable(anyStateInstallButton));
        } catch (StaleElementReferenceException e) {
            moveToElement(toBeInstalledApp);
        }
        wait.until(ExpectedConditions.elementToBeClickable(anyStateInstallButton));
    }

    private void chooseRandomApp(){
        int random = (int) (Math.random() * (apps.size() - 1));
        toBeInstalledApp = apps.get(random);
        System.out.println("The App #" + random + " was selected");
        try {
            moveToElement(toBeInstalledApp);
        } catch (StaleElementReferenceException e) {
            chooseRandomApp();
        }
    }

    private boolean isCompatible() throws InterruptedException {
        try {
//            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
            Thread.sleep(1000);
            return installButton.isDisplayed();
        }
        catch (StaleElementReferenceException e){return false;}
        catch (NoSuchElementException e){return false;}
    }

    public boolean isInstalled(){
        return alreadyInstalledButton.isDisplayed();
    }

    private void filterByApps(){
        appsFilterButton.click();
    }

}
