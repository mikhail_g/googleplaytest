package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class GoogleComPage extends Page {

    public GoogleComPage(WebDriver driver){
            super(driver);
            wait.until(ExpectedConditions.elementToBeClickable(queryField));
    }

    @FindBy(id = "gbqfq")
    private WebElement queryField;

    @FindBy(id = "gb_70")
    private static WebElement loginButton;

    @FindBy(xpath = "//span[text()=\"Play\"]")
    private WebElement googlePlay;
    /*@FindBy(css = "#gb78>span.gb_t")
    private WebElement googlePlay;*/

    //@FindBy(className = "gbwa")
    //private WebElement services;
//    @FindBy(className = "gb_D")
//    private WebElement services;
    @FindBy(css = "a.gb_D.gb_Ta")
    private WebElement services;

    public GoogleLoginPage loginToGoogle() {
        loginButton.click();
        return new GoogleLoginPage(driver);
    }

    public GooglePlayPage goToGooglePlay() throws InterruptedException {
        openServicesMenu();
        selectGooglePlay();
        return new GooglePlayPage(driver);
    }

    private void selectGooglePlay() {
//        WebDriverWait wait = new WebDriverWait(driver, super.propertyLoader.getTimeout());
//        waitForEndOfAllAjaxes();
//        moveToElement(services);
        //wait.until(ExpectedConditions.visibilityOf(googlePlay));
        googlePlay.click();
    }

    private void openServicesMenu() throws InterruptedException {
//        waitForEndOfAllAjaxes();
        //wait.until(ExpectedConditions.elementToBeClickable(services));
        Thread.sleep(1000);
//        moveAndClickOnElement(services);
        services.click();
        System.out.println(services.getAttribute("title"));
    }


}
