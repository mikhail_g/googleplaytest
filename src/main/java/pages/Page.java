package pages;

import framework.util.PropertyLoader;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Page {
    protected WebDriverWait wait;
    protected PropertyLoader propertyLoader = null;
    protected final WebDriver driver;
    protected Actions actions;

    public Page(WebDriver driver){
        this.propertyLoader = PropertyLoader.getInstance();
        this.driver = driver;
        PageFactory.initElements(driver, this);
        actions = new Actions(driver);
        wait = new WebDriverWait(driver, propertyLoader.getTimeout());
    }

    protected void moveToElement(WebElement webElement) {
        try {
            actions.moveToElement(webElement).build().perform();
        } catch (StaleElementReferenceException e) {
            return;
        }

    }

    protected void moveAndClickOnElement(WebElement webElement) {
        try {
            actions.moveToElement(webElement).click().perform();
        } catch (StaleElementReferenceException e) {
            return;
        }

    }

    public void waitForEndOfAllAjaxes(){
        ((JavascriptExecutor)driver).executeScript("myVar = 0; function myFunc() {myVar = 1} setTimeout(myFunc, 0);");
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return (Boolean)((JavascriptExecutor)driver).executeScript("return myVar == 1");
            }
        });
    }
}
