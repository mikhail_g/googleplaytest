package framework.webdriver;

import framework.util.PropertyLoader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class BrowserDriverFactory {
    private final static String browserName = PropertyLoader.getInstance().getBrowserName();  //chrome  or firefox
    private final static String PATH_TO_DRIVER = "\\classes\\drivers";
    private final static String CURRENT_DIR = System.getProperty("user.dir");
    public final static String FIREFOX_NAME = "firefox";
    public final static String CHROME_NAME = "chrome";
    public final static String WRONG_BROWSER_NAME = "!WRONG BROWSER NAME IS SET IN PROPERTIES FILE!";

    public static WebDriver getDriver() throws Exception {
        if (browserName.equalsIgnoreCase(CHROME_NAME)) {
            System.setProperty("webdriver.chrome.driver", CURRENT_DIR + PATH_TO_DRIVER + "\\chromedriver.exe");
            return new ChromeDriver();
        }
        else if(browserName.equalsIgnoreCase(FIREFOX_NAME)){
            return new FirefoxDriver();
        }
        else throw new Exception(WRONG_BROWSER_NAME);
    }


}

