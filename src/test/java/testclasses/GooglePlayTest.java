package testclasses;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.*;
import static org.testng.Assert.assertTrue;


public class GooglePlayTest extends TestBase {

    @BeforeTest
    public void loginToGoogle(){
        GoogleComPage googleComPage = openGoogleComPage();
        GoogleLoginPage googleLoginPage = googleComPage.loginToGoogle();
        googleLoginPage.executeLogin();
    }

    @Test
    public void GooglePlayAppInstallationTest()throws Exception {
        GoogleComPage googleComPageLoggedIn = new GoogleComPage(driver);
        GooglePlayPage googlePlayPage = googleComPageLoggedIn.goToGooglePlay();
        AppPage appPage = googlePlayPage.selectApp();
        appPage.installApp();
        assertTrue(appPage.isInstalled());
    }

    @Test
    public void installChromeAppTest() throws Exception{
        openChromeWebStorePage();
        ChromeWebStorePage chromeWebStorePage = new ChromeWebStorePage(driver);
//        chromeWebStorePage.setFilters();
        chromeWebStorePage.installChromeApp();
        chromeWebStorePage.chooseInstalledApp();
        assertTrue(chromeWebStorePage.isInstalled());
    }
}
